'use strict';

const connectionPromise = require('./connectAMQP');

const queueName = 'tareas';

main().catch(error => { console.log('Hubo un error: ', error) });

async function main(){
    // Conectamos al servidor AMQP
    const conn = await connectionPromise;

    // Conectar un canal
    const channel = await conn.createChannel();

    // Asegurar que la cola existe
    await channel.assertQueue(queueName, {
        durable: true // La cola sobrevive a reinicios del broker
    });

    setInterval(async () => {
        try{
            // Mandar un mensaje
            const message = {
                texto: 'Esta es la tarea creada el ' + Date.now(),
                imagePath: 'ruta',
                maxSize: 500
            }

            const sendAgain = channel.sendToQueue(queueName, Buffer.from(JSON.stringify(message)), {
                persistent: true // El mensaje sobrevive a reinicios del broker
            });

            if(!sendAgain){
                console.log('Esperando a');
                await new Promise(resolve => channel.on('drain', resolve));
            }

            console.log(`Publicado el mensaje: ${message.texto} con resultado ${sendAgain}`);
        }catch(err){
            console.log('Error: ', err);
        }
    }, 500);
}