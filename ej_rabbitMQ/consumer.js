'use strict';

'use strict';

const connectionPromise = require('./connectAMQP');

const queueName = 'tareas';

main().catch(error => { console.log('Hubo un error: ', error) });

async function main(){
    // Conectamos al servidor AMQP
    const conn = await connectionPromise;

    // Conectar un canal
    const channel = await conn.createChannel();

    // Asegurar que la cola existe
    await channel.assertQueue(queueName, {
        durable: true // La cola sobrevive a reinicios del broker
    });

    // Cuántos mensajes simultáneos quiero procesar
    // Si no indicamos esto, hace todos sin esperar
    // el Timeout, ya que es una tarea asíncrona
    channel.prefetch(2);

    // Me suscribo a una cola
    channel.consume(queueName, msg => {
        console.log(msg.content.toString());
        // Hago el trabajo que corresponda a este worker

        // Retardo artificial
        setTimeout(() => {
            // Confirmo a la cola que he procesado el mensaje (acknowlegement)
            // Para que se borre de la cola
            channel.ack(msg);
        }, 500);
    });
}