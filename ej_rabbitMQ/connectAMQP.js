'use strict';

// Importamos librería
const amqplib = require('amqplib');

// Cargamos variables de entorno
require('dotenv').config();

// Conectamos con AMQPLib (devuelve una promesa)
const connectionPromise = amqplib.connect(process.env.RABGITMQ_URL);

module.exports = connectionPromise;