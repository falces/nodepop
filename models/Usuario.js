'use strict';

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const nodemailerTransport = require('../lib/nodemailerConfig');

var usuarioSchema = mongoose.Schema({
    email: { type: String, unique: true },
    password: String,
}, {collection: 'usuarios'}); // Evitamos la pluralización

usuarioSchema.statics.hashPassword = function(plainPassword)
{
    return bcrypt.hash(plainPassword, 10);
}

usuarioSchema.methods.sendEmail = function(_from, _subject, _body){
    // Enviar el correo. Sendmail devuelve una promesa, se la devolvemos
    // a quien haya llamado
    return nodemailerTransport.sendMail({
        from: _from,
        to: this.email, // Cada usuario tiene una propiedad email
        subject: _subject,
        html: _body
    });
}

const Usuario = mongoose.model('usuarios', usuarioSchema);
// Buscará usuarios (pluralizando)

module.exports = Usuario;