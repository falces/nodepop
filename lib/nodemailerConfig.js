'use strict';

const nodemailer = require('nodemailer');

// Crear un transport
const transport = nodemailer.createTransport({
    service: 'SendGrid',
    auth: {
        user: process.env.SENDGRID_USER,
        pass: process.env.SENDGRID_PASSWORD
    }
});

module.exports = transport;