'use strict';

const express = require('express');
const app = express();
const server = require('http').Server(app);

app.get('/', (req, res, next) => {
    res.sendFile(__dirname + '/index.html');
});

server.listen(3001, () => {
    console.log('Listening on port 3000');
});

// Añadimos websockets
const io = require('socket.io')(server);

io.on('connection', socket => {
    // socket = conexión con cada uno de los browsers que tienen la página
    console.log('Nueva conexión de un cliente');

    // Mensaje recibido de un browser
    socket.on('nuevo-mensaje', data => {
        console.log('Mensaje: ', data);
        // Emito a todos los browsers que estén conectados
        io.emit('nuevo-mensaje', data); // No tiene por qué ser igual nombre
    });
/*
    setInterval(() => {
        socket.emit('Pasa un segundo');
    }, 1000);
    */
});