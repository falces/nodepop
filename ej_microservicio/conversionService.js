'use strict';

// Servicio de cambio de moneda

const cote = require('cote');

// Declarar el microservicio
const responder = new cote.Responder({
    name: 'currency responder'
});

// Tabla de conversión --> base de datos del microservicio
const rates = {
    usd_eur: 0.86,
    eur_usd: 1.14
}

// Lógica del microservicio
responder.on('convertir moneda', (req, done) => {
    console.log('servicio: ', req.from, req.to, req.amount, Date.now());
    // Calculamos el resultado
    const result = rates[`${req.from}_${req.to}`] * req.amount;
    done(result);
});